/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.skyarthur.springboot.controller.bean;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private String name;
    private Integer age;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date birthDay;
    private Float salary;
    private Integer version;
}
