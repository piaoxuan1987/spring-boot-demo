/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.skyarthur.springboot.controller;

import java.util.Date;

import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.google.common.base.Strings;
import com.skyarthur.springboot.controller.bean.Person;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/data")
public class DemoController {

    @InitBinder
    public void intDate(WebDataBinder dataBinder) {
        dataBinder.addCustomFormatter(new DateFormatter("yyyyMMdd"));
    }

    @RequestMapping("/demo")
    @ResponseBody
    public String demo(String name) {
        return Strings.isNullOrEmpty(name) ? "demo" : name;
    }

    @RequestMapping("/person")
    @ResponseBody
    public Person getPersonInfo(Date birthDay) {
        return Person.builder().name("skyarthur").age(30).birthDay(birthDay).build();
    }

    @RequestMapping("/model")
    public ModelAndView getModel(@ModelAttribute(value = "person") Person personInfo) {
        log.info("/model request");
        ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());
        modelAndView.addObject(personInfo);
        return modelAndView;
    }

    @RequestMapping("/json")
    public ModelAndView getJsonView() {
        Person person = Person.builder().name("skyarthur").age(30).build();
        ModelAndView modelAndView = new ModelAndView("jsonView");
        modelAndView.addObject(person);
        return modelAndView;
    }
}
