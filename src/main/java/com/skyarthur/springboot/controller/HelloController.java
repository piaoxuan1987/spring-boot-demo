/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.skyarthur.springboot.controller;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skyarthur.springboot.common.utils.JsonUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/test")
public class HelloController {
    @RequestMapping("/hello")
    @ResponseBody
    public String demo(@DateTimeFormat(pattern = "yyyyMMdd") Date date) {
        return "example!";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@RequestParam("names[]") List<String> names) {
        return JsonUtils.toJson(names);
    }
}
