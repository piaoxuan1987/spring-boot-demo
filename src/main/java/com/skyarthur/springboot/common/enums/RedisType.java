package com.skyarthur.springboot.common.enums;

/**
 * Created by skyarthur on 2019-12-21
 */
public enum RedisType {
    PERSON_INFO("person_info");

    private String keyPrefix;

    RedisType(String keyPrefix) {
        this.keyPrefix = keyPrefix;
    }

    public String getRealKey(Object key) {
        return this.keyPrefix + key.toString();
    }
}
