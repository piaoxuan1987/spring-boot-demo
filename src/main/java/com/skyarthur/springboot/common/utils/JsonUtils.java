/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.skyarthur.springboot.common.utils;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * Json工具类
 */
public class JsonUtils {

    private static Gson GSON;
    private static JsonParser PARSER = new JsonParser();

    static {
        GSON = getGB().create();
    }

    private JsonUtils() {
    }

    private static GsonBuilder getGB() {
        GsonBuilder gb = new GsonBuilder().disableHtmlEscaping();
        return gb;
    }

    public static <T> T fromJson(String json, Class<T> classOfT) {
        return GSON.fromJson(json, classOfT);
    }

    @SuppressWarnings("unchecked")
    public static <T> T fromJson(String json, Type type) {
        return (T) GSON.fromJson(json, type);
    }

    public static String toJson(Object src, Type typeOfSrc) {
        return GSON.toJson(src, typeOfSrc);
    }

    public static String toJson(Object o) {
        return GSON.toJson(o);
    }

    public static JsonElement parse(String json) {
        return PARSER.parse(json);
    }
}
