package com.skyarthur.springboot.common;

import com.skyarthur.springboot.common.utils.JsonUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by skyarthur on 2019-12-28
 */

@Slf4j
public class StreamTest {

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class Person {
        private String name;
        private Integer age;
    }

    private List<Person> initPersonList() {
        return Lists.newArrayList(new Person("Tom", 18),
                new Person("Ben", 22),
                new Person("Jack", 16),
                new Person("Hope", 4),
                new Person("Jane", 19),
                new Person("Hope", 16));
    }

    @Test
    public void filterTest() {
        List<Person> personList = initPersonList();
        // 过滤出年龄大于8的数据
        List<Person> result = personList.stream().filter(x -> x.getAge() > 18).collect(Collectors.toList());
        log.info(JsonUtils.toJson(result));
        // filter 链式调用实现 and
        result =
                personList.stream().filter(x -> x.getAge() > 18).filter(x -> x.getName().startsWith("J")).collect(Collectors.toList());
        log.info(JsonUtils.toJson(result));
        // 通过 Predicate 实现 or
        Predicate<Person> con1 = x -> x.getAge() > 18;
        Predicate<Person> con2 = x -> x.getName().startsWith("J");
        result =
                personList.stream().filter(con1.or(con2)).collect(Collectors.toList());
        log.info(JsonUtils.toJson(result));
    }

    @Test
    public void mapTest() {
        List<Person> personList = initPersonList();
        List<String> result = personList.stream().map(Person::getName).collect(Collectors.toList());
        log.info(JsonUtils.toJson(result));
        Set<String> nameSet =
                personList.stream().filter(x -> x.getAge() < 20).map(Person::getName).collect(Collectors.toSet());
        log.info(JsonUtils.toJson(nameSet));
    }

    @Test
    public void flatMapTest() {
        List<Person> personList = initPersonList();
        List<String> result =
                personList.stream().flatMap(x -> Arrays.stream(x.getName().split("n"))).collect(Collectors.toList());
        log.info(JsonUtils.toJson(result));
    }

    @Test
    public void reduceTest() {
        Integer sum = Stream.of(1, 2, 3, 4, 5).reduce(0, Integer::sum);
        Assert.assertEquals(15, sum.intValue());
        sum = Stream.of(1, 2, 3, 4, 5).reduce(10, Integer::sum);
        Assert.assertEquals(25, sum.intValue());
        String result = Stream.of("1", "2", "3")
                .reduce("0", (x, y) -> (x + "," + y));
        log.info(result);
    }

    @Test
    public void collectTest() {
        List<Person> personList = initPersonList();
        // 以name为key, 建立name-person的映射，如果key重复，后者覆盖前者
        Map<String, Person> result = personList.stream().collect(Collectors.toMap(Person::getName, x -> x,
                (x, y) -> y));
        log.info(JsonUtils.toJson(result));
        // 以name为key, 建立name-person_list的映射，即一对多
        Map<String, List<Person>> name2Persons = personList.stream().collect(Collectors.groupingBy(Person::getName));
        log.info(JsonUtils.toJson(name2Persons));
        String name = personList.stream().map(Person::getName).collect(Collectors.joining(",", "{", "}"));
        Assert.assertEquals("{Tom,Ben,Jack,Hope,Jane,Hope}", name);

        // partitioningBy will always return a map with two entries, one for where the predicate is true and one for
        // where it is false. It is possible that both entries will have empty lists, but they will exist.
        List<Integer> integerList = Arrays.asList(3, 4, 5, 6, 7);
        Map<Boolean, List<Integer>> result1 = integerList.stream().collect(Collectors.partitioningBy(i -> i < 3));
        log.info(JsonUtils.toJson(result1));

        result1 = integerList.stream().collect(Collectors.groupingBy(i -> i < 3));
        log.info(JsonUtils.toJson(result1));
    }

    @Test
    public void optionalTest() {
        // 级联判断是否为空
        Person person = new Person();
        person.setName(null);
        Assert.assertFalse(Optional.of(person).map(Person::getName).isPresent());
        Optional.of(person).map(Person::getName).ifPresent(log::info);
        person.setName("Ben");
        person.setAge(5);
        Assert.assertTrue(Optional.of(person).map(Person::getName).isPresent());
        // name如果不为空就打印
        Optional.of(person).map(Person::getName).ifPresent(log::info);
        // old 方法
        if (null != person && null != person.getName()) {
            log.info(person.getName());
        }
        // list判断
        List<Person> personList = initPersonList();
        personList.add(new Person(null, 88));
        Optional.of(personList).map(x -> x.stream().map(Person::getName).collect(Collectors.toList())).ifPresent(x -> log.info(JsonUtils.toJson(x)));
    }

    @Test
    public void parallelTest() {
        List<Integer> dataList = Lists.newArrayList();
        for (Integer i = 0; i <= 10000000; i++) {
            dataList.add(i);
        }
        List<Integer> newDataList = Lists.newArrayList();
        long start;
        long end;
        // old
        start = System.currentTimeMillis();
        for (Integer i : dataList) {
            newDataList.add(i * i);
        }
        end = System.currentTimeMillis();
        log.info("old: {}", end - start);
        newDataList.clear();
        // 同步
        start = System.currentTimeMillis();
        newDataList = dataList.stream().map(x -> x * x).collect(Collectors.toList());
        end = System.currentTimeMillis();
        log.info("new sync: {}", end - start);
        newDataList.clear();
        // 并发
        start = System.currentTimeMillis();
        newDataList = dataList.parallelStream().map(x -> x * x).collect(Collectors.toList());
        end = System.currentTimeMillis();
        log.info("new concurrent: {}", end - start);

    }

    @Test
    public void sortTest(){
        List<Person> personList = initPersonList();
        List<Person> result =
                personList.stream().filter(x->x.getAge() > 4).sorted(Comparator.comparing(Person::getAge)).collect(Collectors.toList());
        log.info(JsonUtils.toJson(result));
        result =
                personList.stream().filter(x->x.getAge() > 4).sorted(Comparator.comparing(Person::getAge).reversed()).collect(Collectors.toList());
        log.info(JsonUtils.toJson(result));
        result =
                personList.stream().filter(x->x.getAge() > 4).sorted(Comparator.comparing(x->x.getName().length())).collect(Collectors.toList());
        log.info(JsonUtils.toJson(result));
    }

}
