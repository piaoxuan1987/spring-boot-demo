package com.skyarthur.springboot.config;

import com.skyarthur.springboot.ApplicationTests;
import com.skyarthur.springboot.common.enums.RedisType;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * Created by skyarthur on 2019-12-21
 */

public class RedisConfigTest extends ApplicationTests {

    @Autowired
    @Qualifier("stringRedisTemplate1")
    private StringRedisTemplate stringRedisTemplate1;

    @Autowired
    @Qualifier("stringRedisTemplate2")
    private StringRedisTemplate stringRedisTemplate2;

    @Test
    public void test() {
        LettuceConnectionFactory factory = (LettuceConnectionFactory) stringRedisTemplate1.getConnectionFactory();
        Assert.assertNotNull(factory);
        Assert.assertEquals(6378, factory.getPort());
        Assert.assertEquals("127.0.0.1", factory.getHostName());
        factory = (LettuceConnectionFactory) stringRedisTemplate2.getConnectionFactory();
        Assert.assertNotNull(factory);
        Assert.assertEquals(6379, factory.getPort());
        Assert.assertEquals("127.0.0.1", factory.getHostName());

        stringRedisTemplate1.opsForValue().set(RedisType.PERSON_INFO.getRealKey("test"), "6378");
        stringRedisTemplate2.opsForValue().set("test", "6379");

        Assert.assertEquals("6378", stringRedisTemplate1.opsForValue().get(RedisType.PERSON_INFO.getRealKey("test")));
        Assert.assertEquals("6379", stringRedisTemplate2.opsForValue().get("test"));


    }
}
